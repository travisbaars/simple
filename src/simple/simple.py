class _Greet:
    def __init__(self) -> None:
        pass

    def _get_greeting(self, greeting: str):
        return f"Hello, {greeting}"

_greeter = _Greet()

def greet(greeting):
    return _greeter._get_greeting(greeting)

def add_one(number):
    return number + 1


if __name__ == "__main__":
    get = input("What is your name? ")

    print(greet(get))