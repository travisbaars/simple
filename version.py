import toml

data = toml.load("pyproject.toml")

# for key, value in data.items():
#     print(f"{key}: {value}")

print(data["project"]["version"])

ver = open("VERSION", "w")

with ver as ver:
    ver.write(data["project"]["version"])
